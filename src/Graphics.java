import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Car;
import model.Model;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    Image carImage = new Image("auto.jpg");
    Image carImage2 = new Image("car.jpg");


    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {

        if (!model.spielVorbei) {

            gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

            gc.setStroke(Color.BLACK);
            gc.fillRect(505, 0, 5, 1000); // Mittelstreifen
            gc.fillRect(495, 0, 5, 1000); // Mittelteifen
            gc.fillRect(350, 0, 5, 100); // Spurtrennung
            gc.fillRect(350, 125, 5, 100); // Spurtrennung
            gc.fillRect(350, 250, 5, 100); // Spurtrennung
            gc.fillRect(350, 375, 5, 100); // Spurtrennung
            gc.fillRect(350, 500, 5, 100); // Spurtrennung
            gc.fillRect(350, 625, 5, 100); // Spurtrennung
            gc.fillRect(350, 750, 5, 100); // Spurtrennung
            gc.fillRect(350, 875, 5, 100); // Spurtrennung
            gc.fillRect(650, 0, 5, 100); // Spurtrennung
            gc.fillRect(650, 125, 5, 100); // Spurtrennung
            gc.fillRect(650, 250, 5, 100); // Spurtrennung
            gc.fillRect(650, 375, 5, 100); // Spurtrennung
            gc.fillRect(650, 500, 5, 100); // Spurtrennung
            gc.fillRect(650, 625, 5, 100); // Spurtrennung
            gc.fillRect(650, 750, 5, 100); // Spurtrennung
            gc.fillRect(650, 875, 5, 100); // Spurtrennung
            gc.fillRect(800, 0, 15, 1000); // Außenstreifen
            gc.fillRect(200, 0, 15, 1000); // Außenstreifen


            for (Car car : this.model.getCars()) {
                gc.drawImage(carImage2, car.getX() - car.getW() / 2, car.getY() - car.getH() / 2, car.getW(), car.getH());
            }

            gc.drawImage(carImage, model.getPlayer().getX(), model.getPlayer().getY() - 52, model.getPlayer().getW(), model.getPlayer().getH());

            gc.setFont(Font.font(35));
            gc.setStroke(Color.BLACK);
            gc.fillText("Score: " + (Model.score), 40, 100);

        } else {
            gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
            gc.setFont(Font.font(100));
            gc.setStroke(Color.BLACK);
            gc.fillText("GAME OVER !", 200, 350);

            gc.setFont(Font.font(50));
            gc.setStroke(Color.BLACK);
            gc.fillText("Score: " + (Model.score), 400, 500);

            gc.setFont(Font.font(60));
            gc.setStroke(Color.BLACK);
            gc.fillText("PRESS SPACE FOR RESTART!", 130, 800);


        }


        //gc.fillText
    }
}
